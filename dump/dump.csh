#!/bin/csh
set pgm=dump

setenv DELLIBS `dellib skelana dstana pxdst vfclap vdclap ux tanagra ufield bsaurus herlib trigger uhlib dstana`
setenv CERNLIBS `cernlib  genlib packlib kernlib ariadne herwig jetset74`

# run patchy
set ycmd=`which nypatchy`
set command="$ycmd - ${pgm}.f $pgm.cra $pgm.ylog - - - ${pgm}.f .go"
echo "Running $command"
eval $command

# compile
foreach  ending ( .f .F)
    ls *$ending >& /dev/null
    if ( $? == 0) then
	foreach file (*$ending)
	    $FCOMP $FFLAGS -c $file
	end
    endif
end

# link
$FCOMP $LDFLAGS *.o -o $pgm.exe $ADDLIB $DELLIBS $CERNLIBS

# execute
./$pgm.exe >&$pgm.log

# cleanup
rm -f *.f *.c *.o
