#! /bin/bash -f
rm pythia.tit
cat >> pythia.tit <<EOF
C-- This is an example Title file for running the PYTHIA generator
C--

C-- turn on FFKEY steering file debug- this can be commented out for production
LIST

C-- Lab ID (used to generate random number seed)
LABO 'CERN'

C-- Run number (used to generate random number seed)
NRUN 1001

C-- Centre of mass energy
ECMS 91.25

C-- Number of events to generate
NEVT 10

C-- Process to generate
C-- See the PYTHIA manual for details, but common ones are:
C-- 1  = Z/gamma
C-- 22 = ZZ
C-- 25 = WW
C-- 35 = Zee (use with caution, double counts with gamma-gamma generators)
C-- 36 = Wev (use with caution, probably unreliable)
ACHAN 22

C-- Allowed Z decays
C-- order is d u s c b e v_e mu v_mu tau v_tau
C-- code is as per MDME(IDC,1) - see PYTHIA manual
C--      0 = off
C--      1 = on
C--      2 = on for particle but off for antiparticle
C--      3 = on for antiparticle but off for particle
C--      4 = for pairs eg W+W-, one but not both is allowed
C--      5 = as =4 but an independent group of channels
C-- eg for taus only ZDK 0 0 0 0 0 0 0 0 0 1 0
ZDK 1 1 1 1 1 1 1 1 1 1 1

C-- Allowed W decays
C--   order is ud us ub cd cs cb ev_e muv_mu tauv_tau
C--   code is as per ZDK above
C-- eg for semileptonic evqq, choose WDK 4 4 4 4 4 4 5 0 0
WDK 1 1 1 1 1 1 1 1 1

C-- Tau polarisation
C-- 0 = Standard JETSET
C-- 1 = use TAUOLA library
TAUP 1

C-- Maximum mass cut on heavy bosons (CKIN 41 and CKIN 43)
C-- default is 12.0 GeV/c**2
C--   minimum is 2.0 GeV/c**2 (hard coded in PYTHIA)
MCUT 2.0

C-- ISR
ISR 1

C-- FSR
FSR 1

C-- Fragmentation
FRAG 1

C-- Coulomb correction
ACOUL 1

C-- Mw
MW 80.41

C-- W width; negative value : calculate internally
GAMW -1.0

C-- Mz
MZ 91.187

C-- Z width
GAMZ 2.490

C-- Mtop
MTOP 173.8

C-- M Charged Higgs
MCHIG 1000.0

C-- M Neutral Higgs
MNHIG 125.36

C-- Normalisation Scheme
C--   MSTP(8) - see manual
C--   0 = running alpha_em(Q**2) and a fixed sin**2 theta_weak
C--   1 = calculate from Gf, Mz, sin**2 theta_weak, Mw
C--   2 = calculate from Gf, Mz, Mw only
NORM 1

C-- Scale for showering of ISR
C--   MSTP(68) switch
C--   0 = Limit to s-hat
C--   1 = scale limited as s for ISR
C--   2 = scale limited to s for ISR and cross-section calulcation
SSCAL 2

C-- Running alpha_em
C-- 0 = fixed at Q**2 = 0
C-- 1 = running alpha_em
C-- 2 = fixed to Q**2 = 0   value below 1 GeV**2;
C--     and to   Q**2 = 2Mw value above 1 GeV**2
C-- 1 is recommended
RUNA 1

C-- Alpha_em(0)
AEM0 137.0359895

C-- Alpha_em(2Mw) (only used if RUNA = 2)
AEMW 128.07

C-- Sin**2 theta_weak
C--   will be ignored if using scheme NORM = 2
S2WE 0.2319

C-- Output flag
C-- 0 = write nothing else use this LUN
LOUT 26

C-- Output format
C-- 0 = DELSIM unformatted
C-- 1 = EXCALIBUR formatted
C-- 2 = FASTSIM formatted
OFORM 0

C-- Number of debug events to print out
NDBG 10

C-- Safety factor on cross sections
C--  increase if "Maxiumum violated" error messages occur
C--  but it slows down the program
SAFE 5.0

END

EOF
make pythia
ln -s pythia.tit fort.11
rm fort.26
pythia | tee pythia$$.log
mv fort.26 pythia.fadgen

# run detector simulation and reconstruction
runsim -VERSION v94c -LABO CERN -NRUN 1000 -EBEAM 45.625 -gext pythia.fadgen
