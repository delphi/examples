#!/bin/bash

# produce 10 jetset events with build-in generator,
# simulate
# reconstruct
# and create short DST output
runsim -VERSION va0u -LABO CERN -NRUN 1000 -EBEAM 45.625 -igen qqps -NEVMAX 10

