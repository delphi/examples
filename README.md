# DELPHI software simple examples
This repository shows basic examples of how to run the software stack and perform various tasks.

## [Creating Monte Carlo samples interactively](scripts/create_mc.sh)

### Internal generators
For creating a few events with a build-in generator run
```
runsim -VERSION va0u -LABO CERN -NRUN 1000 -EBEAM 45.625 -igen qqps -NEVMAX 10

```
This will:
* Create 10 Z-> qqbar events
* at Beam energy 45.625 GeV (ECM 91.25 GeV)
with the build-in QQPS generator.

It will as well pass the events through the detector simulation with the following settings:
* Run number -1000 (negative numbers indicate simulated events)
* Laboratory identifier CERN
* year 2000 (no TPC sector 6 period)
reconstruct them and create an extended short DST file which is ready for analysis.

Created files:
* simana.fadsim : detector simulation output (corresponds to raw data)
* simana.fadana : reconstructed output, full DST format
* simana.xsdst  : short DST output for analysis

Analysis can be run as well on the full DST output in which case a bunch of packages will be rerun.

### External generators
In this case the generator is run externally and the output is written to a file in a specific format. This can then
be passed through the detector simulation with
```
runsim -VERSION va0u -LABO CERN -NRUN 1000 -EBEAM 45.625 -gext generated.lund -NEVMAX 10
```
Old executables for generators can be found in /cvmfs/delphi.cern.ch/mc-production/generators/pgf77_glibc2.2

A source code example of DELPHI tuned Pythia can be found in the [Pythia folder](pythia/README.md).

## [Creating Monte Carlo samples on the HTCondor batch farm at CERN](htcondor/create_mc.sub)
To run the same job on the HTCondor batch farm, you need to follow these steps

### Create a job directory
```
cd ~/
mkdir htcondor
cd htcondor
```

### Create a job script
Wrap the commands you need to run in a script, eg
```
#!/bin/bash

# produce 10 jetset events with build-in generator,
# simulate
# reconstruct
# and create short DST output
runsim -VERSION va0u -LABO CERN -NRUN 1000 -EBEAM 45.625 -igen qqps -NEVMAX 10

```
and save this in a file, say: **create_mc.sh**, and ensure that it is executable:
```
chmod +x create_mc.sh
```

### create the submission file for HTCondor
For HTCondor, unlike LSF, you need to create a submission script which describes the your job.
```
executable            = create_mc.sh
arguments             = $(ClusterID) $(ProcId)
output                = output/create_mc.$(ClusterId).$(ProcId).out
error                 = error/create_mc.$(ClusterId).$(ProcId).err
log                   = log/create_mc.$(ClusterId).log
requirements = (OpSysAndVer =?= "CentOS7")
queue
```
Save this in a file called create_mc.sub, for example.

See [the batch documentation at CERN](https://batchdocs.web.cern.ch) for more details.
The above script will queue your job and ask for resources on CentOS7.

Ensure, that the configured directories exist. In the above example, do
```
mkdir output
mkdir error
mkdir log
```

### Submit the job to the system
Now you can submit your job. In the above example, run
```
kinit
condor_submit create_mc.sub
```
from the htcondor folder. You should see something like:
```
Submitting job(s).
1 job(s) submitted to cluster 2490640.

```

The command
```
condor_q
```
will show you the status of your job, for example:
```

-- Schedd: bigbird13.cern.ch : <137.138.156.150:9618?... @ 11/01/19 13:33:06
OWNER    BATCH_NAME           SUBMITTED   DONE   RUN    IDLE  TOTAL JOB_IDS
schwicke CMD: create_mc.sh  11/1  13:32      _      _      1      1 2490640.0

1 jobs; 0 completed, 0 removed, 1 idle, 0 running, 0 held, 0 suspended
```

Upon job completion, output files will be returned to the submission directory by default, eg:
```
~/htcondor]$ ls -ltr
total 44684
-rwxr-xr-x. 1 schwicke xx      200  1. Nov 13:28 create_mc.sh
-rw-r--r--. 1 schwicke xx      323  1. Nov 13:28 create_mc.sub
drwxr-xr-x. 2 schwicke xx     2048  1. Nov 13:32 output
drwxr-xr-x. 2 schwicke xx     2048  1. Nov 13:32 log
drwxr-xr-x. 2 schwicke xx     2048  1. Nov 13:32 error
-rw-r--r--. 1 schwicke xx      280  1. Nov 13:35 igtots.logn
-rw-r--r--. 1 schwicke xx    10571  1. Nov 13:35 delana.tit
-rwxr-xr-x. 1 schwicke xx 15289420  1. Nov 13:35 simrun36
-rw-r--r--. 1 schwicke xx    30613  1. Nov 13:35 survey.sumr
-rw-r--r--. 1 schwicke xx    11447  1. Nov 13:35 simlocal.title
-rw-r--r--. 1 schwicke xx   460800  1. Nov 13:35 simana.xsdst
-rw-r--r--. 1 schwicke xx  4531200  1. Nov 13:35 simana.fadsim
-rw-r--r--. 1 schwicke xx    14776  1. Nov 13:35 scanlist.sumr
-rw-r--r--. 1 schwicke xx   983040  1. Nov 13:35 simana.fadana
-rw-r--r--. 1 schwicke xx     1725  1. Nov 13:35 delsimrn.out88
-rwxr-xr-x. 1 schwicke xx 24409644  1. Nov 13:35 delana45.exe
```

## [Running an analysis job interactively](scripts/testjob.sh)
The following script can be run interactively or submitted to a batch farm with DELPHI setup

```shell
#!/bin/bash
pgm=skelana

export DELLIBS=`dellib skelana dstana pxdst vfclap vdclap ux tanagra ufield bsaurus herlib trigger uhlib dstana`
export CERNLIBS=`cernlib  genlib packlib kernlib ariadne herwig jetset74`
echo "+OPTION VERbose" > $pgm.cra1
echo "+USE, ${PLINAM}." >> $pgm.cra1
cat $DELPHI_PAM/skelana.cra >> $pgm.cra1

# modify
ycmd=`which nypatchy`
command="$ycmd - $pgm.f $pgm.cra1 $pgm.ylog $pgm.c - - ${pgm}R.f .go"
echo "Running $command"
eval $command

# compile
for ending in .f .F ; do
    ls *$ending >/dev/null 2>&1
    if [ $? -eq 0 ]; then
	for file in *$ending  ; do
	    $FCOMP $FFLAGS -c $file
	done
    fi
done

for ending in  *.c *.C *.cc ; do
    ls *$ending >/dev/null 2>&1
    if [ $? -eq 0 ]; then
	for file in *ending ; do
	    $CCOMP $CFLAGS -c $file
	done
    fi
done

# link
$FCOMP $LDFLAGS *.o -o $pgm.exe $ADDLIB $DELLIBS $CERNLIBS

# cleanup
rm -f *.f *.c *.o

# create input file
echo "FILE = simana.xsdst" > ./PDLINPUT

# execute
./$pgm.exe 1>$pgm.log 2>$pgm.err
```
The C-shell version looks like this:
```shell
#!/bin/csh
set pgm=dump

setenv DELLIBS `dellib skelana dstana pxdst vfclap vdclap ux tanagra ufield bsaurus herlib trigger uhlib dstana`
setenv CERNLIBS `cernlib  genlib packlib kernlib ariadne herwig jetset74`

# run patchy
set ycmd=`which nypatchy`
set command="$ycmd - ${pgm}.f $pgm.cra $pgm.ylog - - - ${pgm}.f .go"
echo "Running $command"
eval $command

# compile
foreach  ending ( .f .F)
    ls *$ending >& /dev/null
    if ( $? == 0) then
	foreach file (*$ending)
	    $FCOMP $FFLAGS -c $file
	end
    endif
end

# link
$FCOMP $LDFLAGS *.o -o $pgm.exe $ADDLIB $DELLIBS $CERNLIBS

# execute
./$pgm.exe >&$pgm.log

# cleanup
rm -f *.f *.c *.o

```

It
* gets the sources
* runs nypatch to create the Fortran input files
* compiles the fortran to create an executable file
* creates a data input fiel which would read **simana.xsdst** from the local folder where the executable will be started
* runs the job

## Running an analysis job on HTCondor
This works exactly as described above. Save the script above to some file, create a submit file and ensure that the exectuable
name matches what you need.

# Additional information
Please check https://gitlab.cern.ch/delphi/quickstart for additional and more accurate information.
