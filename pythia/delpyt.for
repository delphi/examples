      PROGRAM DELPYT
***************************************************************************
***************************************************************************
*
* This program is a main routine for running PYTHIA/JETSET
* 
*         it uses DELPHI tuned JETSET 
*         and produces output in a suitable format for DELSIM
*
*HTP 24/6/98 version increased to 1.22
*    only change is to use same TAUOLA library (v 1.1) as 
*    is used for EXCALIBUR v0.94
*
*
*HTP 6/5/98 minor changes (HEPEVT common -> double precision in taulep2.for
*                         (use JETSET7409_DTAU.FOR which has duplicate
*                         (routines removed
*    version increased to 1.21
*
*HTP 29/4/98 added initialisation for TAUOLA random number generator
*HTP version increased to 1.2
*HTP version 1.1 of DELPYT 
*HTP 25/2/98
*
***************************************************************************
*************************************************************************** 
*HTP version number comment lines added for JETSET and DELPYT 25/2/98
*HTP ability to set choice of scale from steering addedm 09-Feb-98
*HTP rewritten 14-Oct-96
*HTP to use FFREAD input instead of text file
*
      COMMON/LUDAT1/MSTU(200),PARU(200),MSTJ(200),PARJ(200)
      SAVE /LUDAT1/
      COMMON/LUDAT2/KCHG(500,3),PMAS(500,4),PARF(2000),VCKM(4,4)
      SAVE /LUDAT2/
      COMMON/LUDAT3/MDCY(500,3),MDME(2000,2),BRAT(2000),KFDP(2000,5)
      SAVE /LUDAT3/
      COMMON/LUDAT4/CHAF(500)
      CHARACTER CHAF*8
      SAVE /LUDAT4/
      COMMON/LUDATR/MRLU(6),RRLU(100)
      SAVE /LUDATR/
      COMMON/LUJETS/N,K(4000,5),P(4000,5),V(4000,5)
      SAVE /LUJETS/
      COMMON/PYSUBS/MSEL,MSUB(200),KFIN(2,-40:40),CKIN(200)
      SAVE /PYSUBS/
      COMMON/PYPARS/MSTP(200),PARP(200),MSTI(200),PARI(200)
      SAVE /PYPARS/
      COMMON/PYINT5/NGEN(0:200,3),XSEC(0:200,3)
      SAVE /PYINT5/

      DOUBLE PRECISION PI
      PARAMETER (PI=3.14159265358979324D0)
      DOUBLE PRECISION GF
      PARAMETER (GF=1.16639D-5)

* DELPHI tuning COMMON block
      COMMON /sxlprd/ PARJD(200),MSTJD(200)
      SAVE   /sxlprd/

* FFREAD common
      INTEGER NFFR
      REAL SPACE

      PARAMETER (NFFR=5000)
      COMMON /CFREAD/ SPACE(NFFR)

* Steering Common
      
      INTEGER IRANSEED, NRUN, NEVT, ICHAN, IZDK(11), IWDK(9)
      INTEGER ITAUP, ISR, IFSR, IFRAG, ICOUL, NORM, LOUT, NDBG, IRUNA
      INTEGER IOFHTP
      REAL ECMS, AMCUT, AMW, AMZ, AGAMW, AGAMZ, AMTOP, AMCHIG, AMNHIG
      REAL AEM0, AEMW, S2WE, SAFE

      COMMON /HTSTR/ 
     & ECMS, AMCUT, AMW, AMZ, AGAMW, AGAMZ, AMTOP, AMCHIG, AMNHIG,
     & AEM0, AEMW, S2WE, SAFE,
     & IRANSEED, NRUN, NEVT, ICHAN, IZDK, IWDK,
     & ITAUP, ISR, IFSR, IFRAG, ICOUL, NORM, LOUT, NDBG, IRUNA,
     & IOFHTP

*translation array for ZDK and WDK
      INTEGER ITDK(11)
      DATA ITDK/1,2,3,4,5,11,12,13,14,15,16/
      INTEGER ITDK1(9)
      DATA ITDK1/2,2,2,4,4,4,11,13,15/
      INTEGER ITDK2(9)
      DATA ITDK2/1,3,5,1,3,5,12,14,16/


* HEPEVT commons
      integer NMXHEP
      parameter (NMXHEP = 2000)
      integer nevhep, nhep, isthep(NMXHEP), idhep(NMXHEP),
     $     jmohep(2,NMXHEP), jdahep(2,NMXHEP)
      double precision phep(5,NMXHEP), vhep(4,NMXHEP)
      common /hepevt/ nevhep, nhep, isthep, idhep, jmohep, jdahep,
     $     phep, vhep
      double precision shep(4,NMXHEP)
      common /hepspn/ shep

* Lab number stuff
      INTEGER LABID(46)
      INTEGER LABNUM,LABPOS
      CHARACTER*4 LABO
      CHARACTER*184 LABN
      DATA LABN(1:36) /'WIENBELGNBI HELSCDF LAL LPNHSACLSTRA'/
      DATA LABN(37:72) /'KARLWUPPLIVEOXFORAL ATHEANTUDEMOBOLO'/
      DATA LABN(73:108) /'GENOMILAPADUROMATRIETORINIKHBERGOSLO'/
      DATA LABN(109:144) /'CRACWARSSANTVALELUNDSTOCUPPSAMESSERP'/
      DATA LABN(145:164) /'DUBNLISBCERNFARMSNAK'/
      DATA LABN(165:184) /        'LYONGRENMARSCCPNBAST'/
      DATA LABID /110,200,310,410,510,520,530,540,550,610,
     *            620,710,720,730,810,820,830,910,920,930,
     *            940,950,960,970,1010,1110,1120,1210,1220,
     *            1310,1320,1410,1420,1430,1510,1610,1620,
     *            1710,2000,1990,1980,560,570,580,500,590/

*initialize FFREAD

      CALL FFINIT(NFFR)
      CALL FFSET('LINP',11)
      CALL FFSET('LOUT',6)
      CALL FFSET('SIZE',5)

      CALL FFKEY('LABO',  LTEMP,  4,'MIXED')
      CALL FFKEY('ECMS',  ECMS,   1,'REAL')
      CALL FFKEY('NRUN',  NRUN,   1,'INTEGER')
      CALL FFKEY('ISR',   ISR,    1,'INTEGER')
      CALL FFKEY('FSR',   IFSR,   1,'INTEGER')
      CALL FFKEY('FRAG',  IFRAG,  1,'INTEGER')
      CALL FFKEY('ACHAN', ICHAN,  1,'INTEGER')
      CALL FFKEY('ACOUL', ICOUL,  1,'INTEGER')
      CALL FFKEY('NORM',  NORM,   1,'INTEGER')
      CALL FFKEY('NEVT',  NEVT,   1,'INTEGER')
      CALL FFKEY('LOUT',  LOUT,   1,'INTEGER')
      CALL FFKEY('NDBG',  NDBG,   1,'INTEGER')
      CALL FFKEY('ZDK',   IZDK,   11,'INTEGER')
      CALL FFKEY('WDK',   IWDK,   9,'INTEGER')
      CALL FFKEY('TAUP',  TAUP,   1,'INTEGER')
      CALL FFKEY('RUNA',  IRUNA,  1,'INTEGER')
      CALL FFKEY('SSCAL', ISSCAL, 1,'INTEGER')
      CALL FFKEY('MW',    AMW,    1,'REAL')
      CALL FFKEY('MZ',    AMZ,    1,'REAL')
      CALL FFKEY('MTOP' , AMTOP,  1,'REAL')
      CALL FFKEY('MCHIG', AMCHIG, 1,'REAL')
      CALL FFKEY('MNHIG', AMNHIG, 1,'REAL')
      CALL FFKEY('GAMW',  AGAMW,  1,'REAL')
      CALL FFKEY('GAMZ',  AGAMZ,  1,'REAL')
      CALL FFKEY('AEM0',  AEM0,   1,'REAL')
      CALL FFKEY('AEMW',  AEMW,   1,'REAL')
      CALL FFKEY('S2WE',  S2WE,   1,'REAL')
      CALL FFKEY('MCUT',  AMCUT,  1,'REAL')
      CALL FFKEY('SAFE',  SAFE,   1,'REAL')
      CALL FFKEY('OFORM',IOFHTP,  1,'INTEGER')

* read steering
      CALL FFGO

      WRITE(*,*) '***************************************************'
      WRITE(*,*) '***************************************************'
      WRITE(*,*) '**                                               **'
      WRITE(*,*) '**      DELPHI-tuned PYTHIA JETSET PROGRAM       **'
      WRITE(*,*) '**                                               **'
      WRITE(*,*) '**    Original code    by Torbjorn Sjostrand     **'
      WRITE(*,*) '**    Delphi   code    by Hywel Phillips         **'
      WRITE(*,*) '**                                               **'
      WRITE(*,*) '***************************************************'
      WRITE(*,*) '***************************************************'


      CALL UHTOC(LTEMP,4,LABO,4)

 11   FORMAT(F19.8)
 12   FORMAT(I10)
 103  FORMAT(A4)
 1000   FORMAT(/,3X,'*** UPLABO ****  Labname ',A4,' not in lablist')

      LABNUM = 1
      LABPOS = 1

* decode lab ID
      DO 110 IHTP=1,181,4
       KHTP=IHTP+3
       JHTP=KHTP/4
        IF(LABN(IHTP:KHTP) .EQ. LABO) THEN
         LABNUM=LABID(JHTP)
         LABPOS=JHTP
         WRITE(*,*) 'EXCXMP: Laboratory ',LABO,' recognized'
         WRITE(*,*) 'EXCXMP: Lab number set to ',LABNUM
        ENDIF
 110  CONTINUE

      IF(LABNUM.EQ.1) THEN
        WRITE (*,1000) LABO
        STOP
      ENDIF

      IRANSEED=NRUN+10000*LABNUM
      WRITE(*,*) 'EXCXMP: Random number seed set to ',IRANSEED
* random number seed
      MRLU(1)=IRANSEED
*HTP set the TAUOLA seed as well:
      CALL RMARIN(IRANSEED+1,0,0)

* decide which SM parametrisation we are going to use
      IF (NORM.LT.0.OR.NORM.GT.2) THEN
        WRITE(*,*) 'Error: Unknown normalisation scheme: ',NORM
        STOP
      ENDIF           

* normalisation scheme
      MSTP(8)=NORM

*HTP 9/2/98
* choice of scale for ISR QED showers
      MSTP(68)=ISSCAL
      IF (ISSCAL.LT.0.OR.ISSCAL.GT.2) THEN
        WRITE(*,*) 'Error: Unknown choice of shower scale ',ISSCAL
        STOP
      ELSE
        WRITE(*,*) 'Choice for shower scale is ',ISSCAL
      ENDIF 

* alpha_em(0)
      PARU(101)=1.0/AEM0

* sin**2 theta_weak
      IF (NORM.EQ.2) THEN
        PARU(102)=1.0 - AMW**2/AMZ**2
      ELSE
        PARU(102)=S2WE
      ENDIF
      IF (NORM.EQ.2) THEN
        WRITE (*,*) 'Using Gf scheme and calculating Sin**2 theta_w'
        WRITE(*,*) 'Sin** 2 theta_w = ',PARU(102)
      ENDIF
 
* alpha_em (2Mw)
* this value is only significant if IRUNA = 2 is chosen
      PARU(103)=1.0/AEMW

      IF (IRUNA.LT.0.OR.IRUNA.GT.2) THEN
        WRITE(*,*) 'Error: Unknown running alpha_em scheme ',IRUNA
        STOP
      ENDIF
      MSTU(101)=IRUNA

* Mz, Gamma_Z
      PARJ(123)=AMZ
      PARJ(124)=AGAMZ
      PMAS(23,1)=AMZ
      PMAS(23,2)=AGAMZ

* Mw, Gamma_W
      PMAS(24,1)=AMW
      IF (AGAMW.LT.0.0) THEN
        PMAS(24,2)=REAL(3.0d0*GF*(AMW**3)/(2.0d0*PI*SQRT(2.0d0)))
        WRITE (*,*) 'Using calculated W width ',PMAS(24,2)
      ELSE
        PMAS(24,2)=AGAMW
      ENDIF

* Mtop, Mhiggs
      PMAS(6,1) = AMTOP
      PMAS(25,1)= AMNHIG
      PMAS(37,1)= AMCHIG

* SM printout
      WRITE (*,*) 'Standard Model Electroweak parameters'
      WRITE (*,*) 'Using scheme ',MSTP(8)
      WRITE (*,*) 'Running alpha_em mode ',MSTU(101)
      WRITE (*,*) '   Mz            = ',PMAS(23,1)
      WRITE (*,*) '   Gamma_z       = ',PMAS(23,2)
      WRITE (*,*) '   Mw            = ',PMAS(24,1)
      WRITE (*,*) '   Gamma_w       = ',PMAS(24,2)
      WRITE (*,*) '   Alpha_em(0)   = ',PARU(101)
      IF (MSTU(101).EQ.2) THEN
        WRITE (*,*) '   Alpha_em(2Mw) = ',PARU(103)
      ENDIF
      WRITE (*,*) '   sin**2 t_weak = ',PARU(102)
      WRITE (*,*) '   Mtop          = ',PMAS(6,1)
      WRITE (*,*) '   MChiggs       = ',PMAS(25,1)
      WRITE (*,*) '   MNhiggs       = ',PMAS(37,1)

* Coulomb correction
      MSTP(40)= ICOUL

*     tell PYTHIA that I'm going to specify which physics processes to use
      MSEL=0

*     switch on requested process
      MSUB(ICHAN)=1

* mass cut
      WRITE (*,*) 'Mass cut M_boson > ',AMCUT,' applied'
      CKIN(41)=AMCUT
      CKIN(42)=-1.0
      CKIN(43)=AMCUT
      CKIN(44)=-1.0

* safety factor on cross sections
      IF (SAFE.GE.1.0) THEN
        MSTP(121)=1
        PARP(121)=SAFE
        WRITE (*,*) 'Using safety factor ',SAFE
      ENDIF
*     ISR
      IF (ISR.LT.0.OR.ISR.GE.2) THEN
        WRITE(*,*) 'Unknown value of ISR ',ISR
        STOP
      ENDIF
      MSTP(11)=ISR

*     FSR
      IF (IFSR.LT.0.OR.IFSR.GE.2) THEN
        WRITE(*,*) 'Unknown value of FSR ',IFSR
        STOP
      ENDIF
      MSTP(71)=IFSR

*     fragmentation
      IF (IFRAG.LT.0.OR.IFRAG.GE.2) THEN
        WRITE(*,*) 'Unknown value of Fragmentation ',IFRAG
        STOP
      ENDIF
      MSTP(111)=IFRAG

*     decay flag
      MSTJ(22) = 2


* specific steering for different channels

        IF (ICHAN.EQ.1) THEN
* Z/gamma
          CKIN(39)=-1.0
*HTP 9/2/98 read from steering instead
*          MSTP(68)=1
*          WRITE(*,*) 
*     &        'MSTP(68) set to 1- showering limited by s'
        ENDIF

        IF (ICHAN.EQ.35) THEN
* Zee
          MSTP(12)=0
          CKIN(3)=0.01
          CKIN(5)=0.01
          WRITE(*,*) 'CKIN cuts switched on for process 35'
        ENDIF

        IF (ICHAN.EQ.36) THEN
* Wev
          CKIN(3)=0.01
          CKIN(5)=0.01
          WRITE(*,*) 'CKIN cuts switched on for process 36'
        ENDIF


*     initialise
* Delphi tuning
      CALL sxp_jt74_0395

* Particle decays 
* NB it is vital not to turn on combinations with MDME(J,1) = -1
*    or you'll distort the widths!
* Z decays
      ISTART=MDCY(23,2)
      INUM=MDCY(23,3)
      DO J=ISTART,ISTART+INUM-1
        DO L=1,11
          IF (ABS(KFDP(J,1)).EQ.ITDK(L)) THEN
             IF (MDME(J,1).GE.0) THEN
               MDME(J,1)= IZDK(L)
             ENDIF
          ENDIF
        ENDDO
      ENDDO

* W decays
      ISTART=MDCY(24,2)
      INUM=MDCY(24,3)
      DO J=ISTART,ISTART+INUM-1
        DO L=1,9
          IF (ABS(KFDP(J,1)).EQ.ITDK1(L).AND.
     &        ABS(KFDP(J,2)).EQ.ITDK2(L)) THEN 
             IF (MDME(J,1).GE.0) THEN
               MDME(J,1)= IWDK(L)
             ENDIF
          ENDIF
          IF (ABS(KFDP(J,2)).EQ.ITDK1(L).AND.
     &        ABS(KFDP(J,1)).EQ.ITDK2(L)) THEN 
             IF (MDME(J,1).GE.0) THEN
               MDME(J,1)= IWDK(L)
             ENDIF
          ENDIF
        ENDDO
      ENDDO

*formatted IO?
      WRITE (*,*) 'OUTPUT FORMAT: ',IOFHTP,' to unit ',LOUT
      IF (IOFHTP.eq.0) THEN
        OPEN(Unit=LOUT,STATUS='NEW',Form='unformatted',IOSTAT=IOS)
        IF (IOS.NE.0) THEN
          WRITE(*,*) 'DELPYT: Unable to open output file on unit 26'
          STOP
        ENDIF
      ELSE
        OPEN(Unit=LOUT,STATUS='NEW',Form='formatted',IOSTAT=IOS)
        IF (IOS.NE.0) THEN
          WRITE(*,*) 'DELPYT: Unable to open output file on unit 26'
          STOP
        ENDIF
      ENDIF
*HTP debug zero counter
      NEV2G=0

      CALL PYINIT('CMS','e-','e+',ECMS)

* SM printout
      WRITE (*,*) 'Standard Model Electroweak parameters'
      WRITE (*,*) '*** After PYINIT (includes QCD corrections)'
      WRITE (*,*) '   Mz            = ',PMAS(23,1)
      WRITE (*,*) '   Gamma_z       = ',PMAS(23,2)
      WRITE (*,*) '   Mw            = ',PMAS(24,1)
      WRITE (*,*) '   Gamma_w       = ',PMAS(24,2)
      WRITE (*,*) '   Alpha_em(0)   = ',PARU(101)
      IF (MSTU(101).EQ.2) THEN
        WRITE (*,*) '   Alpha_em(2Mw) = ',PARU(103)
      ENDIF
      WRITE (*,*) '   sin**2 t_weak = ',PARU(102)
      WRITE (*,*) '   Mtop          = ',PMAS(6,1)
      WRITE (*,*) '   MChiggs       = ',PMAS(25,1)
      WRITE (*,*) '   MNhiggs       = ',PMAS(37,1)

      WRITE (*,*) 'CKM matrix V(ud)**2',VCKM(1,1)
      WRITE (*,*) 'CKM matrix V(us)**2',VCKM(1,2)
      WRITE (*,*) 'CKM matrix V(ub)**2',VCKM(1,3)
      WRITE (*,*) 'CKM matrix V(cd)**2',VCKM(2,1)
      WRITE (*,*) 'CKM matrix V(cs)**2',VCKM(2,2)
      WRITE (*,*) 'CKM matrix V(cb)**2',VCKM(2,3)
      WRITE (*,*) 'CKM matrix V(td)**2',VCKM(3,1)
      WRITE (*,*) 'CKM matrix V(ts)**2',VCKM(3,2)
      WRITE (*,*) 'CKM matrix V(tb)**2',VCKM(3,3)

      IPOW2=1
      DO INHT=1,NEVT

        CALL PYEVNT

           IF (IOFHTP.EQ.0) THEN
             IF (LOUT.GT.0) THEN
               CALL WINWRITE(LOUT)
             ENDIF
           ELSE IF (IOFHTP.EQ.1) THEN
             IF (LOUT.GT.0) THEN
               CALL WINWRITE2(LOUT)
             ENDIF
           ELSE IF (IOFHTP.EQ.2) THEN
             IF (LOUT.GT.0) THEN
               CALL WINWRITE3(LOUT)
             ENDIF
           ENDIF

*HTP debug - look for Z/gamma events with >1 photon with E>5 GeV
*          NEGAM=0
*          DO I=11,20
*            IF (K(I,2).EQ.22.AND.P(I,4).GE.5.0) THEN
*              NEGAM=NEGAM+1
*            ENDIF
*          ENDDO
*          IF (NEGAM.GE.2) THEN
*            NEV2G=NEV2G+1
*          ENDIF
*HTP debug end          

        IF (INHT.LE.NDBG) THEN
          WRITE (*,*) 'DELPYT Event ',INHT
          CALL LULIST(1)
        ENDIF

        IF(INHT.EQ.IPOW2) THEN
          WRITE (*,*) 'DELPYT Event',INHT
          IPOW2=IPOW2*2
        ENDIF
      ENDDO

      CALL PYSTAT(1)
*HTP debug
*      WRITE(*,*) 'Number of 2 gamma events',NEV2G

      END

      SUBROUTINE WINWRITE(LUN)
*****************************************************************************
*****************************************************************************
*
*     SUBROUTINE WINWRITE(LUN)
*
*     Purpose: Write LUJETS common to output file
*              on unit LUN in format for DELSIM
*
*     Input:   LUN - output file INTEGER
*
*     Output:  None
*
*     Called:  Per event
*HTP: modified 25/06/96 to write out a final comment line of the form
*HTP: K(1) = 21     LUND code for comment
*HTP: K(2) = 0      Special word containing generator info
*HTP: K(3) = 101    PYTHIA generator ID
*HTP: P(1) = 5.722  PYTHIA version number
*HTP added these entries, 25/2/98
*HTP: P(2) = 7.409  DELPHI-tuned JETSET version number
*HTP: P(3) = 1.22    DELPYT (delphi calling code) version number
*
*
*****************************************************************************
*****************************************************************************

*KEEP,LUJETS.
      COMMON/LUJETS/N,K(4000,5),P(4000,5),V(4000,5)
      SAVE /LUJETS/
*KEND.
      INTEGER LUN

*.  formats: (non-portable!!!)
*.        event records, in the following
*.        format (one record per event):
*.
*.    word 0 : n                    (i)
*.         1 : k(1,1)               (i) \
*.         2 : k(1,2)               (i)  \
*.         3 : k(1,3)               (i)   \
*.         4 : k(1,4)               (i)    \
*.         5 : k(1,5)               (i)     \
*.         6 : p(1,1)               (f)      \
*.         7 : p(1,2)               (f)       \
*.         8 : p(1,3)               (f)        >  repeated n times
*.         9 : p(1,4)               (f)       /
*.        10 : p(1,5)               (f)      /
*.        11 : v(1,1)               (f)     /
*.        12 : v(1,2)               (f)    /
*.        13 : v(1,3)               (f)   /
*.        14 : v(1,4)               (f)  /
*.        15 : v(1,5)               (f) /
*.        16 : k(2,1)
*.       ...
*.     n*7-1 : p(n,4)
*.       n*7 : p(n,5)
*
*.
*.----------------------------------------------------------------------

*
*
*   write event data
*
c
C delsim needs    k(Z0 H0 W H+,1)=21
c
        npart=n
        DO 111 ip=1,npart
          ipkf=iabs(k(ip,2))
          IF(ipkf.eq.23.or.ipkf.eq.24.or.ipkf.eq.25.or
     +      .ipkf.eq.37)then
            k(ip,1)=21
          ENDIF
  111   CONTINUE

*HTP add comment line for generator info
      n=n+1
      DO ip=1,5
        K(n,ip)=0
        P(n,ip)=0
        V(n,ip)=0
      ENDDO
      K(n,1)=21
      K(n,2)=0
      K(n,3)=101
      P(n,1)=5.722
*HTP added JETSET and DELPYT version numbers 25/2/98
      P(n,2)=7.409
      P(n,3)=1.22

      write (LUN)
     +     n,((k(i,j),j=1,5),(p(i,j),j=1,5),(v(i,j),j=1,5),i=1,n)

      RETURN
      END
      SUBROUTINE WINWRITE2(LUN)
*****************************************************************************
*****************************************************************************
*
*     SUBROUTINE WINWRITE2(LUN)
*
*     Purpose: Write LUJETS common to output file
*              on unit LUN in EXCALIBUR JETSET commons format
*
*     Input:   LUN - output file INTEGER
*
*     Output:  None
*
*     Called:  Per event
*
*
*****************************************************************************
*****************************************************************************

*     include COMMON blocks
*KEEP,LUJETS.
      COMMON/LUJETS/N,K(4000,5),P(4000,5),V(4000,5)
      SAVE /LUJETS/
*KEEP,LUDAT1.
      COMMON/LUDAT1/MSTU(200),PARU(200),MSTJ(200),PARJ(200)
      SAVE /LUDAT1/
*KEEP,LUDAT2.
      COMMON/LUDAT2/KCHG(500,3),PMAS(500,4),PARF(2000),VCKM(4,4)
      SAVE /LUDAT2/
*KEEP,LUDAT3.
      COMMON/LUDAT3/MDCY(500,3),MDME(2000,2),BRAT(2000),KFDP(2000,5)
      SAVE /LUDAT3/
*KEEP,LUDAT4.
      COMMON/LUDAT4/CHAF(500)
      CHARACTER CHAF*8
      SAVE /LUDAT4/
*KEND.

      INTEGER LUN
*
*   write event data
*

      WRITE(LUN,10) N
      DO I=1,N
        WRITE(LUN,11) (K(I,J),J=1,5)
        WRITE(LUN,12) (P(I,J),J=1,5)
      ENDDO

 10   FORMAT(1I14)
 11   FORMAT(5I14)
 12   FORMAT(5F20.10)
      RETURN
      END

      SUBROUTINE WINWRITE3(LUN)
*****************************************************************************
*****************************************************************************
*
*     SUBROUTINE WINWRITE3(LUN)
*
*     Purpose: Write LUJETS common to output file
*              on unit LUN in FASTSIM JETSET commons format
*
*     Input:   LUN - output file INTEGER
*
*     Output:  None
*
*     Called:  Per event
*   Taken from a routine of Chris Green's from FASTSIM
*
*****************************************************************************
*****************************************************************************

*     include COMMON blocks
*KEEP,LUJETS.
      COMMON/LUJETS/N,K(4000,5),P(4000,5),V(4000,5)
      SAVE /LUJETS/
*KEEP,LUDAT1.
      COMMON/LUDAT1/MSTU(200),PARU(200),MSTJ(200),PARJ(200)
      SAVE /LUDAT1/
*KEEP,LUDAT2.
      COMMON/LUDAT2/KCHG(500,3),PMAS(500,4),PARF(2000),VCKM(4,4)
      SAVE /LUDAT2/
*KEEP,LUDAT3.
      COMMON/LUDAT3/MDCY(500,3),MDME(2000,2),BRAT(2000),KFDP(2000,5)
      SAVE /LUDAT3/
*KEEP,LUDAT4.
      COMMON/LUDAT4/CHAF(500)
      CHARACTER CHAF*8
      SAVE /LUDAT4/
*KEND.

      INTEGER LUN
*
*   write event data
*
      WRITE(LUN,6301,ERR=999) N
 6301 FORMAT(I6)
C
      DO I = 1, N
         DO J = 1,5
            WRITE(LUN,6302,ERR=999)K(I,J),P(I,J),V(I,J)
         ENDDO
      ENDDO
C     * 26/07/96 CG * Changed to I10 -- some K values large!
 6302 FORMAT(I10,2F16.8)

      RETURN
 999  WRITE(*,*) 'WINWRITE3: Error writing LUND file to unit ',LUN
      STOP
      END
